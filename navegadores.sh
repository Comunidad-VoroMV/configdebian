#!/bin/bash
echo  Indica que navegador deseas instalar:
    sleep 1s
    echo
    echo   1: "Ultima versión de Firefox (Rama estable)"
    echo   2: Google chrome
    echo   3: Brave Browser
    echo   4: Microsoft Edge
    echo   5. Chromium
    echo   6: Opera
    echo   7: Vivaldi
    echo   8: Tor Browser
    echo   n: Cancelar
    echo ""
    sleep 1s
    read -p "Opcion: " sn
    echo
    case $sn in
            [1]* )  wget "https://download.mozilla.org/?product=firefox-latest&os=linux64&lang=es-ES" -O latest-firefox.tar.bz2
                    rm -r /opt/firefox
                    tar -jxvf latest-firefox.tar.bz2 -C /opt
                    echo 'Firefox instalado'
                    ln -s /opt/firefox/firefox /usr/bin/firefox
                    touch test.desktop
                    echo -e "[Desktop Entry]\nName=Firefox\nComment=Navegador web\nGenericName=Web Browser\nX-GNOME-FullName=Firefox ''Su versión'' Web Browser\nExec=/opt/firefox/firefox %u\nTerminal=false\nX-MultipleArgs=false\nType=Application\nIcon=/opt/firefox/browser/chrome/icons/default/default128.png\nCategories=Network;WebBrowser;\nMimeType=text/html;text/xml;application/xhtml+xml;application/xml;application/vnd.mozilla.xul+xml;application/rss+xml;application/rdf+xml;image/gif;image/jpeg;image/png;x-scheme-handler/http;x-scheme-handler/https;\nStartupWMClass=Firefox\nStartupNotify=true\nPath=" > test.desktop
                    mv test.desktop /usr/share/applications/firefox.desktop
                    rm latest-firefox.tar.bz2

   		    sleep 3s ;;

            [2]* )  echo deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main >> /etc/apt/sources.list.d/google-chrome.list ; sudo wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - ; apt update -y && apt install -y google-chrome-stable
  		    sleep 3s ;;

            [3]* )  sudo apt install -y apt-transport-https curl gnupg
                    curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | sudo apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-release.gpg add -
                    echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main" | sudo tee /etc/apt/sources.list.d/brave-browser-release.list
                    sudo apt update ; sudo apt install -y brave-browser
		            echo ""  
  		    sleep 3s ;;

            [4]* )  sudo apt install -y apt-transport-https ca-certificates curl software-properties-common wget -y
                    sudo wget -O- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor | sudo tee /usr/share/keyrings/microsoft-edge.gpg
                    echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/microsoft-edge.gpg] https://packages.microsoft.com/repos/edge stable main' | sudo tee /etc/apt/sources.list.d/microsoft-edge.list
                    sudo apt update ; sudo apt install -y microsoft-edge-stable
		            echo ""  
  		    sleep 3s ;;

            [5]* )  sudo apt install -y chromium
		            echo ""  
  		    sleep 3s ;;

            [6]* )  sudo add-apt-repository 'deb https://deb.opera.com/opera-stable/ stable non-free'
                    wget -qO- https://deb.opera.com/archive.key | sudo apt-key add -
                    sudo apt update ; sudo apt install -y opera-stable
		            echo ""  
  		    sleep 3s ;;

            [7]* )  wget -qO- https://repo.vivaldi.com/archive/linux_signing_key.pub | gpg --dearmor | sudo dd of=/usr/share/keyrings/vivaldi-browser.gpg
                    echo "deb [signed-by=/usr/share/keyrings/vivaldi-browser.gpg arch=$(dpkg --print-architecture)] https://repo.vivaldi.com/archive/deb/ stable main" | sudo dd of=/etc/apt/sources.list.d/vivaldi-archive.list
                    sudo apt update ; sudo apt install -y vivaldi-stable                    
                            echo ""
                    sleep 3s ;;

            [8]* )  sudo apt install -y torbrowser-launcher
                            echo ""
                    sleep 3s ;;

            [Nn]* ) echo " Cancelando..."
		    exit;;

            * ) echo "Por favor, elige una opción o pulsa n para salir del menú."
		echo ;;
    esac