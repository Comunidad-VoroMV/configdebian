#!/bin/bash
echo
 echo "************************************************************************"
 echo "**                                                                    **"
 echo "**                           CONFIGDEBIAN                             **"  
 echo "**                                                                    **"
 echo "************************************************************************"
echo
echo "Este es el script de Roberto Díaz, para configurar tu Debian 11."
sleep 2s
echo
echo ¡Vamos a ello!
sleep 3s
echo 
echo "Empezemos por el principio xD, los repositorios y añadir al usuario al grupo sudo"
sleep 1s
echo
echo "$USER ALL=(ALL) ALL" >> /etc/sudoers
cat /dev/null > /etc/apt/sources.list
echo "#------------------------------------------------------------------------------#" >> /etc/apt/sources.list
echo "#                            OFFICIAL DEBIAN REPOS                             #" >> /etc/apt/sources.list
echo "#------------------------------------------------------------------------------#" >> /etc/apt/sources.list
echo "                          ###### Debian Main Repos ######" >> /etc/apt/sources.list
echo "" >> /etc/apt/sources.list
echo "deb http://deb.debian.org/debian/ bullseye main contrib non-free" >> /etc/apt/sources.list
echo "deb-src http://deb.debian.org/debian/ bullseye main contrib non-free" >> /etc/apt/sources.list
echo "deb http://security.debian.org/debian-security bullseye-security main contrib non-free" >> /etc/apt/sources.list
echo "deb-src http://security.debian.org/debian-security bullseye-security main contrib non-free" >> /etc/apt/sources.list
echo "deb http://deb.debian.org/debian bullseye-updates main contrib non-free" >> /etc/apt/sources.list
echo "deb-src http://deb.debian.org/debian bullseye-updates main contrib non-free" >> /etc/apt/sources.list
#echo "## bullseye-debian-multimedia##" >> /etc/apt/sources.list
#echo "deb http://www.deb-multimedia.org bullseye main non-free" >> /etc/apt/sources.list
#apt-get update -oAcquire::AllowInsecureRepositories=true
#apt-get install -y deb-multimedia-keyring

echo
echo "Ahora vamos a actualizar el sistema"
sleep 1s
echo
apt update -y && apt upgrade -y && apt autoremove -y && apt autoclean -y && apt clean -y
echo
echo "Ya tienes todo actualizado y limpio, ahora puedes crear un archivo swap (esto es un reemplazo de la partición swap en caso de que no la hayas creado porque tengas un SSD por ejemplo"
sleep 1s
echo

 read -p "Quieres crear un swapfile (s/n)?" sn
    case $sn in
        [Ss]* )  sudo fallocate -l 2G /swapfile ;
 sudo ls -lh /swapfile ;
 sudo chmod 600 /swapfile ;
 sudo ls -lh /swapfile ;
 sudo mkswap /swapfile ;
 sudo swapon /swapfile ;
 sudo swapon -s ;
 echo "/swapfile       none    swap    sw      0       0" >> /etc/fstab;;

        [Nn]* ) echo;;
        * ) echo "Por favor, pulsa s o n.";;
    esac

echo
echo Instalamos los drivers.
echo
apt install -y firmware-linux firmware-linux-free firmware-linux-nonfree firmware-misc-nonfree
echo
echo "Ya tienes instalado el firmware de Debian, para que todos los dispositivos funcionen correctamente."
sleep 3s
echo
echo "Puede instalar el driver privativo de nvidia si quieres (en el caso de que tengas gráfica de nvidia y no quieras usar nouveau)"
echo
sleep 1s

read -p "Quieres instalar el driver de privativo de nvidia (s/n)?" sn
    case $sn in
        [Ss]* )  apt install -y nvidia-detect ;
                 nvidia-detect ;
 driver=$(nvidia-detect | grep "nvidia-" | sed 's/ //g' ) ; apt install "$driver";;
        [Nn]* ) echo;;
        * ) echo "Por favor, pulsa s o n.";;
    esac

echo
echo Ahora si quieres puedes instalar flatpak para tener una fuente de software mucho mayor en tu sistema.
echo

read -p "Quieres instalar flatpak (s/n)?" sn
if [ "$sn" = s ] 
        then
case $XDG_CURRENT_DESKTOP in
    GNOME)
        apt install flatpak -y ; apt install -y gnome-software-plugin-flatpak ; flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
        ;;
    X-Cinnamon)
        apt install flatpak -y ; apt install -y gnome-software-plugin-flatpak ; flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
        ;;
    KDE)
        apt install -y flatpak ; apt install -y plasma-discover-backend-flatpak ; flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
        ;;
    *)
        apt install -y flatpak ; flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
        ;;
         esac
elif [ "$sn" = n ]
then
echo ""
fi
echo
echo "También puedes instalar snap si lo necesitas"
echo

read -p "Quieres instalar snap (s/n)?" sn
if [ "$sn" = s ]
        then
case $XDG_CURRENT_DESKTOP in
    GNOME)
        apt install -y snapd gnome-software-plugin-snap
        ;;
    X-Cinnamon)
        apt install -y snapd gnome-software-plugin-snap
        ;;
    KDE)
        apt install -y snapd plasma-discover-backend-snap
        ;;
    *)
        apt install -y snapd
        ;;
         esac
elif [ "$sn" = n ]
then
echo ""
fi
echo


        read -p "Quieres instalar algunas aplicaciones (s/n)?" sn
if [ "$sn" = s ]
        then
case $XDG_CURRENT_DESKTOP in 
    GNOME)
        apt install -y linux-headers-$(uname -r|sed 's/[^-]*-[^-]*-//') build-essential make automake cmake autoconf git aptitude nautilus-share curl gdebi curl gparted chrome-gnome-shell inxi locate --fix-missing --fix-broken --fix-policy
        ;;
    KDE)
        apt install -y linux-headers-$(uname -r|sed 's/[^-]*-[^-]*-//') build-essential make automake cmake autoconf git aptitude synaptic curl ffmpegthumbs curl gparted locate --fix-missing --fix-broken --fix-policy
        ;;
    *)
        apt install -y linux-headers-$(uname -r|sed 's/[^-]*-[^-]*-//') build-essential make automake cmake autoconf git aptitude synaptic curl curl cmake gparted locate --fix-missing --fix-broken --fix-policy
        ;;
         esac
elif [ "$sn" = n ]
then
echo ""
fi
echo


        read -p "¿Quieres instalar algún navegador de internet extra como Chrome, Brave...etc? (s/n)?" sn
if [ "$sn" = s ]
        then
./navegadores.sh
         
elif [ "$sn" = n ]
then
echo ""
fi

echo

case $XDG_CURRENT_DESKTOP in
    GNOME)
        read -p "Quieres eliminar los juegos de gnome (s/n)?" sn
        case $sn in
            [Ss]* )  sudo apt purge -y gnome-games && sudo apt autoremove -y gnome-games ;;
            [Nn]* ) echo;;
                * ) echo "Por favor, pulsa s o n.";;
        esac
        ;;
*) echo
        ;;
esac

echo

case $XDG_CURRENT_DESKTOP in
    GNOME)
        echo "Ahora podrías instalar extensiones de gnome si quieres, este es el enlace donde puedes encontrarlas: https://extensions.gnome.org/" ; echo
        ;;
    *)
        echo
        ;;
esac

sleep 3s
echo Actualizamos el grub, para reconozca si tienes dual boot con otros sistemas operativos y en ese caso te permita arrancarlos.
echo
sleep 1s
sudo update-grub
echo
echo ¡Ya hemos terminado, tu Debian esta listo!
sleep 2s
echo

echo "Este script es gracias a mi trabajo personal y cumple las cuatro libertades del software libre, que son las siguientes:"
echo ""
echo "La libertad de ejecutar el software como te plazca y con cualquier objetivo."
echo "La libertad de estudiar como funciona el programa y cambiarlo a tu gusto."
echo "La libertad de poder redistribuir copias del programa a los demás."
echo "La libertad de poder distribuir también tus mejoras al programa original."

echo
echo "Gracias por usar mi script"
echo
echo "Por favor, reinicia el sistema para todos los cambios se apliquen correctamente"
sleep 3s
echo

    read -p "Quieres reiniciar el equipo (s/n)?" sn
    case $sn in
        [Ss]* )  sudo reboot;;
        [Nn]* ) exit;;
        * ) echo "Por favor, pulsa s o n.";;
    esac
done